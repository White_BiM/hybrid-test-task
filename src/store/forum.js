/* eslint-disable no-console,no-unused-vars */

Storage.prototype.setObj = function(key, obj) {
    return this.setItem(key, JSON.stringify(obj))
}
Storage.prototype.getObj = function(key) {
    return JSON.parse(this.getItem(key))
}

function formatDate(date) {

    let dd = date.getDate();
    if (dd < 10) dd = '0' + dd;

    let mm = date.getMonth() + 1;
    if (mm < 10) mm = '0' + mm;

    let yy = date.getFullYear() % 100;
    if (yy < 10) yy = '0' + yy;

    let hh = date.getHours();
    if (hh < 10) hh = '0' + hh;

    let mim = date.getMinutes();
    if (mim < 10) mim = '0' + mim;

    return dd + '.' + mm + '.' + yy + ' ' + hh + ':' + mim;
}
let generateUID = function () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace( /[xy]/g, function ( c ) {
        var r = Math.random() * 16 | 0;
        return ( c == 'x' ? r : ( r & 0x3 | 0x8 ) ).toString( 16 );
    } );
};
class Thread {
    constructor(title, briefContent, content, threadId = null, creationDate = '', comments = []){
        this.title = title
        this.briefContent = briefContent
        this.content = content
        this.threadId = threadId
        this.creationDate = creationDate
        this.comments = comments
    }
}

class Comment {
    constructor(comment, authorName, creationDate = '', commentId = null) {
        this.comment = comment
        this.authorName = authorName
        this.creationDate = creationDate
        this.commentId = commentId
    }
}

export default {
    state: {
        threads: []
    },
    mutations: {
        addThread(state, payload){
            state.threads.push(payload)
            let threads = state.threads
            localStorage.setObj('threads', threads)
        },
        loadThreads(state, payload){
            if (payload) {
                Object.keys(payload).forEach(key => {
                    if (payload[key]) {
                        state.threads.push(payload[key])
                    }
                })
            }
        },
        deleteThread(state, threadId){
            const myArray = state.threads.filter(function( obj ) {
                return obj.threadId !== threadId;
            })
            state.threads = myArray
            localStorage.setObj('threads', state.threads)
        },
        updateThread(state, {title, briefContent, content, threadId}){
            const thread = state.threads.find(a => {
                return a.threadId === threadId
            })
            thread.title = title
            thread.content = content
            thread.briefContent = briefContent
            localStorage.setObj('threads', state.threads)
        },
        addComment(state, [newComment, threadId]){
            const thread = state.threads.find(a => {
                return a.threadId === threadId
            })
            thread.comments.push(newComment)
            localStorage.setObj('threads', state.threads)
        },
        updateComment(state, {comment, commentId, threadId}){
            const thread = state.threads.find(a => {
                return a.threadId === threadId
            })
            const thisComment = thread.comments.find(a => {
                return a.commentId === commentId
            })
            thisComment.comment = comment
            localStorage.setObj('threads', state.threads)
        },
        deleteComment(state, {commentId, threadId}){
            const thread = state.threads.find(a => {
                return a.threadId === threadId
            })
            const myArray = thread.comments.filter(function( obj ) {
                return obj.commentId !== commentId;
            })
            thread.comments = myArray
            localStorage.setObj('threads', state.threads)
        },
    },

    actions: {
        async fetchThreads({commit}){
            let resultThreads = []
            try{
                let threadsArr = localStorage.getItem('threads');
                resultThreads = JSON.parse(threadsArr)
                commit ('loadThreads', resultThreads)
            } catch(error){
                throw error
            }
        },
        async createNewThread ({commit, state}, {title, briefContent, content}){
            const threadDate = formatDate(new Date())
            const threadId = generateUID()
            try {
                const newThread = new Thread(
                    title,
                    briefContent,
                    content,
                    threadId,
                    threadDate,
                    []
                )
                commit ('addThread', newThread)
            } catch (error) {
                throw error
            }
        },
        async deleteThread({commit}, threadId){
            try {
               commit('deleteThread', threadId)
            } catch(error){
                throw error
            }
        },
        async updateThread({commit}, {title, briefContent, content, threadId}){
            try {
                commit ('updateThread', {title, briefContent, content, threadId})
            } catch(error){
                throw error
            }
        },
        async addComment({commit}, {comment, authorName, threadId}){
            const commentDate = formatDate(new Date())
            const commentId = generateUID()
            const newComment = new Comment(
                comment,
                authorName,
                commentDate,
                commentId
            )
            try {
                commit ('addComment', [newComment, threadId])
            } catch(error){
                throw error
            }
        },
        async updateComment({commit}, {comment, commentId, threadId}){
            try {
                commit ('updateComment', {comment, commentId, threadId})
            } catch(error){
                throw error
            }
        },
        async deleteComment({commit}, {commentId, threadId}){
            try {
                commit('deleteComment', {commentId, threadId})
            } catch(error){
                throw error
            }
        },
    },

    getters:  {
        threads (state) {
            return state.threads
        },
        threadById (state) {
            return threadId => {
                return state.threads.find(thread => thread.threadId === threadId)
            }
        },
        /*comments (state) {
            return state.comments
        },
        commentById (state) {
            return commentId => {
                return state.comments.find(comment => comment.commentId === commentId)
            }
        },*/
    }
}