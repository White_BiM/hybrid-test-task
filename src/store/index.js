import Vue from "vue"
import Vuex from 'vuex'
import forum from './forum'
Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        forum
    }

})
