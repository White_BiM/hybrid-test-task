import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex';
import vuetify from './plugins/vuetify'
import VueRouter from 'vue-router'
import router from './router'
import store from './store'
import NewThreadModal from '@/components/Forum/NewThreadModal'
import UpdateThreadModal from '@/components/Forum/UpdateThreadModal'
import DeleteThreadModal from '@/components/Forum/DeleteThreadModal'
import UpdateCommentModal from '@/components/Forum/UpdateCommentModal'
import DeleteCommentModal from '@/components/Forum/DeleteCommentModal'
Vue.component('new-thread-modal', NewThreadModal)
Vue.component('update-thread-modal', UpdateThreadModal)
Vue.component('delete-thread-modal', DeleteThreadModal)
Vue.component('update-comment-modal', UpdateCommentModal)
Vue.component('delete-comment-modal', DeleteCommentModal)

Vue.use(Vuex, VueRouter);
Vue.config.productionTip = false

new Vue({
    vuetify,
    render: h => h(App),
    router,
    store,
    created() {
        this.$store.dispatch('fetchThreads')
    }
}).$mount('#app')
