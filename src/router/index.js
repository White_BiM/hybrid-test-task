import Vue from 'vue'
import Router from 'vue-router'
import Forum from '@/components/Forum/Forum'
import Thread from '@/components/Forum/Thread'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        { path: '/', redirect: '/Forum' },
        {
            path: '/Forum',
            props: true,
            name: 'Forum',
            component: Forum
        },
        {
            path: '/thread/:id',
            props: true,
            name: 'Thread',
            component: Thread
        },
    ],
    scrollBehavior(){
        return{x:0,y:0}
    }
})